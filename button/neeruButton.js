document.addEventListener('DOMContentLoaded',()=>{
  const render = function (node,color) {
    if (color===null || color===""){
      color="#00135B"
    }
    nodeLoc = document.querySelector(node);
    let template =
      '<div id="neeru-button" ><button onclick="paymentPost()" target="popup" id="neeru-payment"><img src="https://firebasestorage.googleapis.com/v0/b/punto7b-3910c.appspot.com/o/pagored_189x36.png?alt=media&token=452a3128-6cc7-48a9-861b-b58101c708af" alt=""></button></div>';
    nodeLoc.innerHTML = template;
    //Estilos para el tag div class neeru-button
    let neeruDivButton = document.getElementById('neeru-button')
    neeruDivButton.style.width="100%"
    neeruDivButton.style.display="flex"
    // Estilos para el tag button
    let neeruButton = document.getElementById('neeru-payment')
    neeruButton.style.backgroundColor=color
    neeruButton.style.borderRadius="18px"
    neeruButton.style.border="none"
    neeruButton.style.width="100%"
    neeruButton.style.maxHeight="80px"
    neeruButton.style.padding="10px"
  
  };
  
  let stylesColor = document.getElementById("Neerus").getAttribute("data-color")
  
  render("#neeru-payment-button",stylesColor);
  
  
  
})

function paymentPost() {
  const domainURL = "https://dev.gcf-corp.com:107";
  const domainRoute = "gateway";
  let scriptValues = document.getElementById("Neerus");

  let urlreference = scriptValues.getAttribute("data-reference");
  let urlDescription = scriptValues.getAttribute("data-description")
  let urlRif = scriptValues.getAttribute("data-rif")
  let urlRifParsed = parseDni(urlRif)
  let urlRifNumber = (urlRifParsed != null && urlRifParsed != "") ? urlRifParsed.substring(1) : ""

 
// Validaciones de inputs y tamaños

  if (scriptValues.getAttribute("data-apikey")==null || scriptValues.getAttribute("data-apikey")===""){
    alert("No ha sido configurado el valor de data-apikey, por favor agregue una apikey en data-apikey del script")
  }
  
  if (scriptValues.getAttribute("data-amount")==null || scriptValues.getAttribute("data-amount")===""){
    alert("No ha sido configurado el valor de data-amount, por favor agregue una monto válido en data-amount del script")
  }

  if ((urlRifNumber != null && urlRifNumber != "")  && (urlRifNumber.length < 5 || urlRifNumber.length >10)) {
    return alert('El número de RIF es incorrecto , por favor ingrese un número entre  5  y 10 dígitos')
  }

  if ((urlreference != null && urlreference != "") && urlreference.length > 15) {
   return alert('La referencia excede el máximo de caracteres permitidos, por favor ingrese un máximo 15 caracteres')

  }

  if ((urlDescription != null && urlDescription != "") && urlDescription.length > 30) {
   return alert('La descripción excede el máximo de caracteres permitidos, por favor ingrese un máximo 30 caracteres')
  }


  
  if (urlDescription==null || urlDescription===""){
    urlDescription="void"
  }

  if (urlreference==null || urlreference===""){
    urlreference="void"
  }
  
  if (urlRif==null || urlRif===""){
    urlRif="void"
  }


  const value = {
    apikey: scriptValues.getAttribute("data-apikey"),
    amount: parseAmount(scriptValues.getAttribute("data-amount")),
    reference: parseString(urlreference),
    description: parseString(urlDescription),
    dni: parseDni(urlRif),
  };
  
  
  
  if ((value.apikey != null && value.apikey != "") && (value.amount != null && value.amount != "")){
    let encryptedAmount = encodeURIComponent(
      CryptoJS.AES.encrypt(value.amount, "neerusSecret")
    );
    const url = `${domainURL}/${domainRoute}/${value.apikey}/${encryptedAmount}/${value.reference}/${value.description}/${value.dni}`;
    let neeruGateWay=window.open(url, "popup", 
    `width=600,
    height=600`);

   
  }
  
}


function parseAmount (amount) {
  // Tomamos el monto 
  let amountBp=amount;

  // Separamos el monto en parte entera y decimal DEBEN SER DOS DECIMALES
  // Para la parte entera las peraciones deben ser . por eso reemplazamos todos los , por .
  let notDecimalPart = amountBp.slice(0,amountBp.length-3).replaceAll(',','.')
  // Para la parte decimal es inverso, todos las . seran reemplazadas por ,
  let DecimalPart = amountBp.slice(-3).replaceAll('.',',')
  // Finalmente las concatenamos y pasamos el nuevo monto.
  let newAmount=notDecimalPart + DecimalPart
  

  return newAmount
}

function parseString (string) {
  // Tomamos el monto 
  let stringBp=string;

  // Separamos el monto en parte entera y decimal DEBEN SER DOS DECIMALES
  // Para la parte entera las peraciones deben ser . por eso reemplazamos todos los , por .
  let newString = stringBp.replaceAll(' ','[]')
  

  

  return newString
}

function parseDni (dni) {
  // Tomamos el monto 
  let dniFlag = dni != null ? dni : "" 
  let stringBp=dniFlag;

  // Separamos el monto en parte entera y decimal DEBEN SER DOS DECIMALES
  // Para la parte entera las peraciones deben ser . por eso reemplazamos todos los , por .
  let newString = stringBp.replaceAll('-','')
  

  

  return newString
}
